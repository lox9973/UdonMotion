Shader "Unlit/Overlay" {
Properties {
	_MainTex("MainTex", 2D) = "white" {}
	_Color("Color", Color) = (1,1,1,1)
}
SubShader {
	Tags { "Queue"="Overlay" "RenderType"="Overlay" }
	Pass {
		Tags { "LightMode"="ForwardBase" }
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile_instancing
#include <UnityCG.cginc>

sampler2D _MainTex;
float4 _MainTex_ST;
float4 _Color;

struct VertInput {
	float3 vertex  : POSITION;
	float2 uv      : TEXCOORD0;
	UNITY_VERTEX_INPUT_INSTANCE_ID
};
struct FragInput {
	float2 tex : TEXCOORD1;
	float4 pos : SV_Position;
	UNITY_VERTEX_OUTPUT_STEREO
};

void vert(VertInput i, out FragInput o) {
	UNITY_SETUP_INSTANCE_ID(i);
	UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
	o.pos = UnityObjectToClipPos(i.vertex);
	o.pos.z = lerp(o.pos.z, o.pos.w, 0.99);
	o.tex = i.uv * _MainTex_ST.xy + _MainTex_ST.zw;
}
float4 frag(FragInput i) : SV_Target {
	float3 sample = tex2Dlod(_MainTex, float4(i.tex, 0, 0));
	return float4(sample * _Color.rgb, 1);
}
ENDCG
	}
}
}