# UdonMotion

## A motion tracking system for VRChat world

UdonMotion is a player motion tracking system for VRChat world, written in [UdonSharp](https://github.com/MerlinVR/UdonSharp). It is able to capture the motion of **any player** with reasonable humanoid armature in the instance without interrupting the player, and retarget the animation onto a puppet avatar in the world. It is designed to be used with [ShaderMotion](https://gitlab.com/lox9973/ShaderMotion), which allows to stream fullbody motion across VR platforms.

## Installation

- Require Unity (2019.4.29f1+), UdonSharp (v0.20.3+), VRCSDK3-WORLD (2021.11.24+).
- Download the latest [release](../../releases) zip file.
- Extract the zip file into `Assets/UdonMotion` in your Unity project.

## Getting started

This project contains a working example scene `Example/MotionTracker.unity` which gives an overview of the whole system.