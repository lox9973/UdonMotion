#if UDON
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UdonSharp;
namespace UdonMotion {
[UdonBehaviourSyncMode(BehaviourSyncMode.None)]
public class MotionTracker : UdonSharpBehaviour {
	// a simple interface of player selection
	public  UnityEngine.UI.Text playerName;
	private VRCPlayerApi[] players = new VRCPlayerApi[90];
	public void PrevPlayer() {
		VRCPlayerApi.GetPlayers(players);
		var n = VRCPlayerApi.GetPlayerCount();
		var i = Mathf.Min(System.Array.IndexOf(players, sourcePlayer), n-1);
		if(n > 0)
			sourcePlayer = players[(i-1+n+n) % n];
		if(Utilities.IsValid(sourcePlayer) && Utilities.IsValid(playerName))
			playerName.text = sourcePlayer.displayName;
	}
	public void NextPlayer() {
		VRCPlayerApi.GetPlayers(players);
		var n = VRCPlayerApi.GetPlayerCount();
		var i = Mathf.Min(System.Array.IndexOf(players, sourcePlayer), n-1);
		if(n > 0)
			sourcePlayer = players[(i+1) % n];
		if(Utilities.IsValid(sourcePlayer) && Utilities.IsValid(playerName))
			playerName.text = sourcePlayer.displayName;
	}

	// the source is the player
	public  VRCPlayerApi sourcePlayer;
	public  Transform    sourceRoot;
	private Vector3[]    sourceTs;
	private Quaternion[] sourceQs;
	private Vector3      sourceT_Root;
	private Quaternion   sourceQ_Root;
	private Quaternion[] sourcePostQs;
	private float        sourceScale;

	// the target is the puppet
	public  Animator     targetAnimator;
	private Transform[]  targetBones;
	private Transform    targetRoot;
	private Quaternion[] targetPostQs, targetRestLQs;
	private Vector3      targetUnitScale;

	// the handles are attached to source bones (for example, as indicators)
	public  Transform    handleRoot;
	public  GameObject   handlePrefab;
	private Transform[]  handles;

	private void Start() {
		this.enabled = false; // turn off tracking by default

		sourceTs = new Vector3[BoneCount];
		sourceQs = new Quaternion[BoneCount];
		sourcePostQs = new Quaternion[BoneCount];
		targetPostQs = new Quaternion[BoneCount];
		targetRestLQs = new Quaternion[BoneCount];
		InitializeTarget();

		handles = new Transform[BoneCount];
		for(int i=0; i<BoneCount; i++) {
			if(handleRoot)
				handles[i] = handleRoot.Find(((HumanBodyBones)i).ToString());
			if(handlePrefab && !handles[i]) {
				handles[i] = VRCInstantiate(handlePrefab).transform;
				handles[i].SetParent(handleRoot??transform, false);
			}
		}
	}

	public  float calibrationCooldown = 0.1f;
	private float lastCalibrationTime;
	private Vector2 lastRigVector;

	// calibrate player bone axes
	public void Calibrate() {
		if(!this.enabled)
			return;
		if(object.ReferenceEquals(sourcePlayer, null))
			sourcePlayer = Networking.LocalPlayer; // convenience: track local user by default
		if(!Utilities.IsValid(sourcePlayer))
			return;
		Debug.Log($"[MotionTracker] Calibrate {sourcePlayer.displayName}");

		UpdateSource();
		CalibrateQ(true);
		CalibrateScale();
		ResetAndApplyTargetTQS();
		lastCalibrationTime = Time.time;
		lastRigVector = new Vector2(
			Vector3.Distance(sourceTs[LeftShoulder], sourceTs[RightShoulder]),
			Vector3.Distance(sourceTs[LeftUpperLeg], sourceTs[RightUpperLeg]));
	}

	// transfer player motion to puppet when the behaviour is enabled
	public void Update() {
		if(!(this.enabled && Utilities.IsValid(sourcePlayer)))
			return;
		UpdateSource();
		var rigVector = new Vector2(
			Vector3.Distance(sourceTs[LeftShoulder], sourceTs[RightShoulder]),
			Vector3.Distance(sourceTs[LeftUpperLeg], sourceTs[RightUpperLeg]));
		if(Vector2.Distance(rigVector, lastRigVector) > 1e-2f && Time.time-lastCalibrationTime > calibrationCooldown)
			Calibrate();
		else
			ApplyTargetTQ();
	}

	// call this if you change the target
	public void InitializeTarget() {
		if(!Utilities.IsValid(targetAnimator))
			return;
		targetRoot = targetAnimator.transform;
		targetBones = new Transform[HumanTrait.BoneCount];
		for(int i=0; i<HumanTrait.BoneCount; i++)
			targetBones[i] = targetAnimator.GetBoneTransform((HumanBodyBones)i);

		sourceT_Root = targetRoot.position;
		sourceQ_Root = targetRoot.rotation;
		for(int i=0; i<BoneCount; i++) if(targetBones[i]) {
			sourceTs[i] = targetBones[i].position;
			sourceQs[i] = targetBones[i].rotation;
		}
		CalibrateQ(true);
		CalibrateScale();

		targetUnitScale = targetRoot.localScale / sourceScale;
		for(int i=0; i<BoneCount; i++) if(targetBones[i]) {
			targetPostQs[i] = sourcePostQs[i];
			targetRestLQs[i] = targetBones[i].localRotation;
		}
	}

	void UpdateSource() {
		sourceT_Root = sourcePlayer.GetPosition();
		sourceQ_Root = sourcePlayer.GetRotation();
		for(int i=0; i<BoneCount; i++) {
			sourceTs[i] = sourcePlayer.GetBonePosition((HumanBodyBones)i);
			sourceQs[i] = sourcePlayer.GetBoneRotation((HumanBodyBones)i);
		}
	}

	Quaternion zeroQuat = new Quaternion(0,0,0,0);
	int[] indexFingerBones = new int[]{
		LeftIndexProximal, LeftIndexIntermediate, LeftIndexDistal,
		RightIndexProximal, RightIndexIntermediate, RightIndexDistal};
	void ApplyTargetQ() {
		var preQ = targetRoot.rotation * Quaternion.Inverse(sourceRoot.rotation);
		for(int i=0; i<BoneCount; i++) {
			var q = sourceQs[i] * sourcePostQs[i];
			if(!zeroQuat.Equals(q)) {
				if(handles[i])
					handles[i].SetPositionAndRotation(sourceTs[i], q);
				if(targetBones[i])
					targetBones[i].rotation = preQ * q * Quaternion.Inverse(targetPostQs[i]);
			}
		}
		// propagate finger rotations
		foreach(var IndexFinger in indexFingerBones) {
			var MiddleFinger = IndexFinger-LeftIndexProximal+LeftMiddleProximal;
			var RingFinger   = IndexFinger-LeftIndexProximal+LeftRingProximal;
			var LittleFinger = IndexFinger-LeftIndexProximal+LeftLittleProximal;
			if(zeroQuat.Equals(sourcePostQs[IndexFinger]) || !targetBones[IndexFinger])
				continue;
			if(zeroQuat.Equals(sourcePostQs[MiddleFinger]))
				targetBones[MiddleFinger].localRotation = targetBones[IndexFinger].localRotation;
			if(zeroQuat.Equals(sourcePostQs[RingFinger]))
				targetBones[RingFinger].localRotation = targetBones[MiddleFinger].localRotation;
			if(zeroQuat.Equals(sourcePostQs[LittleFinger]))
				targetBones[LittleFinger].localRotation = targetBones[RingFinger].localRotation;
			if(zeroQuat.Equals(sourcePostQs[RingFinger]))
				targetBones[RingFinger].localRotation = targetBones[LittleFinger].localRotation;
		}
	}
	void ApplyTargetTQ() {
		ApplyTargetQ();
		var hipsLT = sourceRoot.InverseTransformPoint(sourceTs[Hips]);
		targetBones[Hips].position = targetRoot.position + targetRoot.rotation * hipsLT;
	}
	void ResetAndApplyTargetTQS() {
		for(int i=0; i<BoneCount; i++)
			if(targetBones[i])
				targetBones[i].localRotation = targetRestLQs[i];
		targetRoot.localScale = sourceScale * targetUnitScale;
		ApplyTargetTQ();
		for(int i=0; i<BoneCount; i++)
			if(handles[i])
				handles[i].gameObject.SetActive(!zeroQuat.Equals(sourcePostQs[i]));
	}

	const int Hips = (int)HumanBodyBones.Hips;
	const int LeftUpperLeg = (int)HumanBodyBones.LeftUpperLeg;
	const int RightUpperLeg = (int)HumanBodyBones.RightUpperLeg;
	const int LeftLowerLeg = (int)HumanBodyBones.LeftLowerLeg;
	const int RightLowerLeg = (int)HumanBodyBones.RightLowerLeg;
	const int LeftFoot = (int)HumanBodyBones.LeftFoot;
	const int RightFoot = (int)HumanBodyBones.RightFoot;
	const int Spine = (int)HumanBodyBones.Spine;
	const int Chest = (int)HumanBodyBones.Chest;
	const int UpperChest = (int)HumanBodyBones.UpperChest;
	const int Neck = (int)HumanBodyBones.Neck;
	const int Head = (int)HumanBodyBones.Head;
	const int LeftShoulder = (int)HumanBodyBones.LeftShoulder;
	const int RightShoulder = (int)HumanBodyBones.RightShoulder;
	const int LeftUpperArm = (int)HumanBodyBones.LeftUpperArm;
	const int RightUpperArm = (int)HumanBodyBones.RightUpperArm;
	const int LeftLowerArm = (int)HumanBodyBones.LeftLowerArm;
	const int RightLowerArm = (int)HumanBodyBones.RightLowerArm;
	const int LeftHand = (int)HumanBodyBones.LeftHand;
	const int RightHand = (int)HumanBodyBones.RightHand;
	const int LeftToes = (int)HumanBodyBones.LeftToes;
	const int RightToes = (int)HumanBodyBones.RightToes;
	const int LeftEye = (int)HumanBodyBones.LeftEye;
	const int RightEye = (int)HumanBodyBones.RightEye;
	const int Jaw = (int)HumanBodyBones.Jaw;
	const int LeftThumbProximal = (int)HumanBodyBones.LeftThumbProximal;
	const int LeftThumbIntermediate = (int)HumanBodyBones.LeftThumbIntermediate;
	const int LeftThumbDistal = (int)HumanBodyBones.LeftThumbDistal;
	const int LeftIndexProximal = (int)HumanBodyBones.LeftIndexProximal;
	const int LeftIndexIntermediate = (int)HumanBodyBones.LeftIndexIntermediate;
	const int LeftIndexDistal = (int)HumanBodyBones.LeftIndexDistal;
	const int LeftMiddleProximal = (int)HumanBodyBones.LeftMiddleProximal;
	const int LeftMiddleIntermediate = (int)HumanBodyBones.LeftMiddleIntermediate;
	const int LeftMiddleDistal = (int)HumanBodyBones.LeftMiddleDistal;
	const int LeftRingProximal = (int)HumanBodyBones.LeftRingProximal;
	const int LeftRingIntermediate = (int)HumanBodyBones.LeftRingIntermediate;
	const int LeftRingDistal = (int)HumanBodyBones.LeftRingDistal;
	const int LeftLittleProximal = (int)HumanBodyBones.LeftLittleProximal;
	const int LeftLittleIntermediate = (int)HumanBodyBones.LeftLittleIntermediate;
	const int LeftLittleDistal = (int)HumanBodyBones.LeftLittleDistal;
	const int RightThumbProximal = (int)HumanBodyBones.RightThumbProximal;
	const int RightThumbIntermediate = (int)HumanBodyBones.RightThumbIntermediate;
	const int RightThumbDistal = (int)HumanBodyBones.RightThumbDistal;
	const int RightIndexProximal = (int)HumanBodyBones.RightIndexProximal;
	const int RightIndexIntermediate = (int)HumanBodyBones.RightIndexIntermediate;
	const int RightIndexDistal = (int)HumanBodyBones.RightIndexDistal;
	const int RightMiddleProximal = (int)HumanBodyBones.RightMiddleProximal;
	const int RightMiddleIntermediate = (int)HumanBodyBones.RightMiddleIntermediate;
	const int RightMiddleDistal = (int)HumanBodyBones.RightMiddleDistal;
	const int RightRingProximal = (int)HumanBodyBones.RightRingProximal;
	const int RightRingIntermediate = (int)HumanBodyBones.RightRingIntermediate;
	const int RightRingDistal = (int)HumanBodyBones.RightRingDistal;
	const int RightLittleProximal = (int)HumanBodyBones.RightLittleProximal;
	const int RightLittleIntermediate = (int)HumanBodyBones.RightLittleIntermediate;
	const int RightLittleDistal = (int)HumanBodyBones.RightLittleDistal;
	const int BoneCount = (int)HumanBodyBones.LastBone;

	void CalibrateScale() {
		sourceScale = Mathf.Max(EstimateHipsH(LeftUpperLeg, LeftLowerLeg, LeftFoot),
								EstimateHipsH(RightUpperLeg, RightLowerLeg, RightFoot));
	}
	float EstimateHipsH(int UpperLeg, int LowerLeg, int Foot) {
		var hipsQ     = sourceQs[Hips] * sourcePostQs[Hips];
		var upperLegQ = sourceQs[UpperLeg] * sourcePostQs[UpperLeg];
		var lowerLegQ = sourceQs[LowerLeg] * sourcePostQs[LowerLeg];
		var hipsV     = sourceTs[UpperLeg] - sourceTs[Hips];
		var upperLegV = sourceTs[LowerLeg] - sourceTs[UpperLeg];
		var lowerLegV = sourceTs[Foot]     - sourceTs[LowerLeg];
		return Vector3.Dot(hipsQ     * Vector3.down, hipsV)
		     + Vector3.Dot(upperLegQ * Vector3.down, upperLegV)
		     + Vector3.Dot(lowerLegQ * Vector3.down, lowerLegV)
		     + Vector3.Dot(sourceQ_Root * Vector3.down, sourceT_Root - sourceTs[Foot]);
	}

	// neutral pose of mixamo rig
	Quaternion upperLegPreQ = Quaternion.Euler(-30, 0, 0);
	Quaternion lowerLegPreQ = Quaternion.Euler(+80, 0, 0);
	Quaternion footPreQ     = Quaternion.Euler(+20, 0, 0);
	Quaternion leftUpperArmPreQ  = Quaternion.AngleAxis(new Vector3(0, +30, +40).magnitude,
														new Vector3(0, +30, +40).normalized);
	Quaternion rightUpperArmPreQ = Quaternion.AngleAxis(new Vector3(0, -30, -40).magnitude,
														new Vector3(0, -30, -40).normalized);
	Quaternion leftLowerArmPreQ  = Quaternion.Euler(0, +80, 0);
	Quaternion rightLowerArmPreQ = Quaternion.Euler(0, -80, 0);
	Quaternion leftProximalPreQ       = Quaternion.Euler(0, 0, +35);
	Quaternion rightProximalPreQ      = Quaternion.Euler(0, 0, -35);
	Quaternion leftThumbProximalPreQ  = Quaternion.AngleAxis(new Vector3(-10, +60, +20).magnitude,
															new Vector3(-10, +60, +20).normalized);
	Quaternion rightThumbProximalPreQ = Quaternion.AngleAxis(new Vector3(-10, -60, -20).magnitude,
															new Vector3(-10, -60, -20).normalized);
	// empirical weights
	const float upperArmQLerp = 0.3f;
	const float lowerArmQLerp = 0.7f;
	const float footZLerp     = 0.7f;

	void CalibrateQ(bool strict) {
		if(strict)
			for(int i=0; i<BoneCount; i++)
				sourcePostQs[i] = new Quaternion(0,0,0,0);

		CalibrateQ_Backbone();
		CalibrateQ_Head(strict);
		CalibrateQ_Leg(LeftUpperLeg,  LeftLowerLeg,  LeftFoot,  LeftToes,  strict);
		CalibrateQ_Leg(RightUpperLeg, RightLowerLeg, RightFoot, RightToes, strict);
		CalibrateQ_Arm(LeftShoulder, LeftUpperArm, LeftLowerArm, LeftHand,
			LeftIndexProximal, LeftMiddleProximal, LeftRingProximal, LeftLittleProximal,
			Vector3.left, leftUpperArmPreQ, leftLowerArmPreQ);
		CalibrateQ_Arm(RightShoulder, RightUpperArm, RightLowerArm, RightHand,
			RightIndexProximal, RightMiddleProximal, RightRingProximal, RightLittleProximal,
			Vector3.right, rightUpperArmPreQ, rightLowerArmPreQ);

		CalibrateQ_Finger(LeftHand, LeftThumbProximal,  LeftThumbIntermediate,  LeftThumbDistal,
			Vector3.left, leftThumbProximalPreQ);
		CalibrateQ_Finger(LeftHand, LeftIndexProximal,  LeftIndexIntermediate,  LeftIndexDistal,
			Vector3.left, leftProximalPreQ);
		CalibrateQ_Finger(LeftHand, LeftMiddleProximal, LeftMiddleIntermediate, LeftMiddleDistal,
			Vector3.left, leftProximalPreQ);
		CalibrateQ_Finger(LeftHand, LeftRingProximal,   LeftRingIntermediate,   LeftRingDistal,
			Vector3.left, leftProximalPreQ);
		CalibrateQ_Finger(LeftHand, LeftLittleProximal, LeftLittleIntermediate, LeftLittleDistal,
			Vector3.left, leftProximalPreQ);

		CalibrateQ_Finger(RightHand, RightThumbProximal,  RightThumbIntermediate,  RightThumbDistal,
			Vector3.right, rightThumbProximalPreQ);
		CalibrateQ_Finger(RightHand, RightIndexProximal,  RightIndexIntermediate,  RightIndexDistal,
			Vector3.right, rightProximalPreQ);
		CalibrateQ_Finger(RightHand, RightMiddleProximal, RightMiddleIntermediate, RightMiddleDistal,
			Vector3.right, rightProximalPreQ);
		CalibrateQ_Finger(RightHand, RightRingProximal,   RightRingIntermediate,   RightRingDistal,
			Vector3.right, rightProximalPreQ);
		CalibrateQ_Finger(RightHand, RightLittleProximal, RightLittleIntermediate, RightLittleDistal,
			Vector3.right, rightProximalPreQ);
	}
	void CalibrateQ_Head(bool strict) {
		var noEye = Vector3.zero.Equals(sourceTs[LeftEye]) || Vector3.zero.Equals(sourceTs[RightEye]);
		if(strict) {
			var headX = sourceTs[RightEye] - sourceTs[LeftEye];
			var headY = sourceQs[Head] * Vector3.up;
			var headZ = sourceQ_Root * Vector3.forward;
			if(Vector3.zero.Equals(headX))
				headX = sourceQs[Head] * Vector3.right;
			// TODO: fix the hack here
		#if UDON
			if(sourcePlayer != null && sourcePlayer.isLocal)
				CalibrateQ_SnapRot(Head, Vector3.up, Vector3.forward,
					sourcePlayer.GetTrackingData(VRCPlayerApi.TrackingDataType.Head).rotation);
			else
		#endif
			if(noEye)
				CalibrateQ_SnapVec(Head, Vector3.up, Vector3.forward, headY, headZ);
			else
				CalibrateQ_SnapVec(Head, Vector3.right, Vector3.forward, headX, headZ);
		}
		if(!noEye) {
			var headQ = sourceQs[Head] * sourcePostQs[Head];
			// calibrate eyes from head
			CalibrateQ_SnapRot(LeftEye,  Vector3.forward, Vector3.up, headQ);
			CalibrateQ_SnapRot(RightEye, Vector3.forward, Vector3.up, headQ);
		}
	}
	void CalibrateQ_Backbone() {
		var hipsV  = sourceTs[Spine] - sourceTs[Hips];
		var spineV = sourceTs[Chest] - sourceTs[Spine];
		var chestV = sourceTs[Neck]  - sourceTs[Chest]; // this is approximation due to possible upperChest
		var neckV  = sourceTs[Head]  - sourceTs[Neck];
		hipsV += spineV * 0.01f; // take care of zero-length hips bone

		// calibrate hips & chest from lower & upper triangles
		var hipsQ    = CalibrateQ_SnapVec(Hips, Vector3.up, Vector3.right,
			hipsV,  sourceTs[RightUpperLeg] - sourceTs[LeftUpperLeg]);
		var chestQ   = CalibrateQ_SnapVec(Chest, Vector3.up, Vector3.right,
			chestV, sourceTs[RightShoulder] - sourceTs[LeftShoulder]);
		// calibrate spine from hips & chest
		var spineQ_0 = QuatLookAt(hipsQ,  Vector3.up, spineV);
		var spineQ_1 = QuatLookAt(chestQ, Vector3.up, spineV);
		var spineQ   = CalibrateQ_SnapRot(Spine, Vector3.up, Vector3.right, Quaternion.Slerp(spineQ_0, spineQ_1, 0.5f));
		// calibrate neck from chest
		var neckQ_0  = QuatLookAt(chestQ, Vector3.up, neckV);
		var neckQ    = CalibrateQ_SnapRot(Neck, Vector3.up, Vector3.right, neckQ_0);
	}
	void CalibrateQ_Leg(int UpperLeg, int LowerLeg, int Foot, int Toes, bool strict) {
		var hipsQ     = sourceQs[Hips] * sourcePostQs[Hips];
		var upperLegV = sourceTs[LowerLeg] - sourceTs[UpperLeg];
		var lowerLegV = sourceTs[Foot]     - sourceTs[LowerLeg];
		var footV     = sourceTs[Toes]     - sourceTs[Foot];
		if(Vector3.zero.Equals(sourceTs[Toes]))
			footV = Vector3.zero;

		// calibrate upperLeg from hips
		var upperLegQ_0 = QuatLookAt(hipsQ * upperLegPreQ, Vector3.down, upperLegV);
		var upperLegQ   = CalibrateQ_SnapRot(UpperLeg, Vector3.down, Vector3.right, upperLegQ_0);
		// calibrate lowerLeg from upperLeg
		var lowerLegQ_0 = QuatLookAt(upperLegQ * lowerLegPreQ, Vector3.down, lowerLegV);
		var lowerLegQ   = CalibrateQ_SnapRot(LowerLeg, Vector3.down, Vector3.right, lowerLegQ_0);
		if(strict) {
			var grounded = true; // TODO
			// calibrate foot from lowerLeg & toes & ground plane
			var footZ = Vector3.Lerp(lowerLegQ * Vector3.forward, footV.normalized, footZLerp);
			var footY = (grounded ? sourceQ_Root : lowerLegQ) * Vector3.up;
			CalibrateQ_FreeVec(Foot, Vector3.up, Vector3.forward, footY, footZ);
		}
	}
	void CalibrateQ_Arm(int Shoulder, int UpperArm, int LowerArm, int Hand,
			int IndexProximal, int MiddleProximal, int RingProximal, int LittleProximal,
			Vector3 axisOut, Quaternion upperArmPreQ, Quaternion lowerArmPreQ) {
		if(Vector3.zero.Equals(sourceTs[RingProximal])) {
			RingProximal = LittleProximal;
			if(Vector3.zero.Equals(sourceTs[RingProximal]))
				RingProximal = MiddleProximal;
		}
		var chestQ    = sourceQs[Chest] * sourcePostQs[Chest];
		var shoulderV = sourceTs[UpperArm] - sourceTs[Shoulder];
		var upperArmV = sourceTs[LowerArm] - sourceTs[UpperArm];
		var lowerArmV = sourceTs[Hand]     - sourceTs[LowerArm];
		var indexV    = sourceTs[IndexProximal] - sourceTs[Hand];
		var ringV     = sourceTs[RingProximal]  - sourceTs[Hand];
		var noFinger  = Vector3.zero.Equals(sourceTs[IndexProximal]) || Vector3.zero.Equals(sourceTs[RingProximal]);

		// calibrate hand from hand triangle
		var handQ       = CalibrateQ_SnapVec(Hand, axisOut, Vector3.forward, indexV + ringV, indexV - ringV);
		// calibrate shoulder from chest
		var shoulderQ_0 = QuatLookAt(chestQ, axisOut, shoulderV);
		var shoulderQ   = CalibrateQ_SnapRot(Shoulder, axisOut, Vector3.forward, shoulderQ_0);
		// calibrate upperArm from untwisted shoulder & lowerArm
		var lowerArmQ_1 = QuatLookAt(handQ,  axisOut, lowerArmV);
		var upperArmQ_0 = QuatLookAt(shoulderQ_0 * upperArmPreQ, axisOut, upperArmV);
		var upperArmQ_1 = QuatLookAt(lowerArmQ_1 * Quaternion.Inverse(lowerArmPreQ), axisOut, upperArmV);
		var upperArmQ   = CalibrateQ_SnapRot(UpperArm, axisOut, Vector3.forward,
			noFinger ? upperArmQ_0 : Quaternion.Slerp(upperArmQ_0, upperArmQ_1, upperArmQLerp));
		// calibrate lowerArm from upperArm & hand
		var lowerArmQ_0 = QuatLookAt(upperArmQ * lowerArmPreQ, axisOut, lowerArmV);
		var lowerArmQ   = CalibrateQ_SnapRot(LowerArm, axisOut, Vector3.forward,
			noFinger ? lowerArmQ_0 : Quaternion.Slerp(lowerArmQ_0, lowerArmQ_1, lowerArmQLerp));
		// calibrate hand from lowerArm when finger is missing
		if(noFinger)
			handQ       = CalibrateQ_SnapRot(Hand, axisOut, Vector3.forward, lowerArmQ);
	}
	void CalibrateQ_Finger(int Hand, int Proximal, int Intermediate, int Distal,
			Vector3 axisOut, Quaternion proximalPreQ) {
		if(Vector3.zero.Equals(sourceTs[Proximal]))
			return;
		var handQ     = sourceQs[Hand] * sourcePostQs[Hand];
		var proximalV = sourceTs[Intermediate] - sourceTs[Proximal];
		if(Vector3.zero.Equals(sourceTs[Intermediate]))
			proximalV = Vector3.zero;

		// calibrate proximal from hand
		var proximalQ_0 = QuatLookAt(handQ * proximalPreQ, axisOut, proximalV);
		var proximalQ   = CalibrateQ_FreeSnapRot(Proximal, axisOut, Vector3.up, proximalQ_0);
		// reuse proximal calibration on intermediate & distal 
		if(!Vector3.zero.Equals(sourceTs[Intermediate]))
			sourcePostQs[Intermediate] = sourcePostQs[Proximal];
		if(!Vector3.zero.Equals(sourceTs[Distal]))
			sourcePostQs[Distal] = sourcePostQs[Proximal];
	}

	Vector3 SnapAxis(Vector3 vec) {
		var vecAbs = Vector3.Max(vec, -vec);
		var maxAbs = Mathf.Max(vecAbs.x, vecAbs.y, vecAbs.z);
		return maxAbs == vecAbs.x ? Vector3.right * Mathf.Sign(vec.x) :
				maxAbs == vecAbs.y ? Vector3.up * Mathf.Sign(vec.y) : Vector3.forward * Mathf.Sign(vec.z);
	}
	Quaternion QuatLookAt(Quaternion q, Vector3 d, Vector3 v) {
		return Quaternion.FromToRotation(q*d, v) * q;
	}
	Quaternion CalibrateQ_LocalLookAt(int boneId, Vector3 axis0, Vector3 axis1, Vector3 lvec0, Vector3 lvec1) {
		var postQ = Quaternion.LookRotation(lvec0, lvec1) * Quaternion.Inverse(Quaternion.LookRotation(axis0, axis1));
		sourcePostQs[boneId] = postQ;
		return sourceQs[boneId] * postQ;
	}
	Quaternion CalibrateQ_FreeVec(int boneId, Vector3 axis0, Vector3 axis1, Vector3 vec0, Vector3 vec1) {
		var inv = Quaternion.Inverse(sourceQs[boneId]);
		return CalibrateQ_LocalLookAt(boneId, axis0, axis1, inv * vec0, inv * vec1);
	}
	Quaternion CalibrateQ_SnapVec(int boneId, Vector3 axis0, Vector3 axis1, Vector3 vec0, Vector3 vec1) {
		var inv = Quaternion.Inverse(sourceQs[boneId]);
		var lvec0 = SnapAxis(inv * vec0);
		var lvec1 = SnapAxis(Vector3.ProjectOnPlane(inv * vec1, lvec0));
		return CalibrateQ_LocalLookAt(boneId, axis0, axis1, lvec0, lvec1);
	}
	Quaternion CalibrateQ_SnapRot(int boneId, Vector3 axis0, Vector3 axis1, Quaternion rot) {
		var inv = Quaternion.Inverse(sourceQs[boneId]); rot = inv * rot;
		var lvec0 = SnapAxis(rot * axis0);
		var lvec1 = SnapAxis(Quaternion.FromToRotation(rot * axis0, lvec0) * (rot * axis1));
		return CalibrateQ_LocalLookAt(boneId, axis0, axis1, lvec0, lvec1);
	}
	Quaternion CalibrateQ_FreeSnapRot(int boneId, Vector3 axis0, Vector3 axis1, Quaternion rot) {
		var inv = Quaternion.Inverse(sourceQs[boneId]); rot  = inv * rot;
		var lvec0 = (rot * axis0);
		var lvec1 = SnapAxis(rot * axis1);
		return CalibrateQ_LocalLookAt(boneId, axis0, axis1, lvec0, lvec1);
	}
}
}
#endif