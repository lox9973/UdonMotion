﻿#if UDON
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UdonSharp;
namespace UdonMotion {
[UdonBehaviourSyncMode(BehaviourSyncMode.None)]
public class MotionExporter : UdonSharpBehaviour {
	// the avatar to track
	public Animator animator;
	public bool keepOriginalPositionXZ = true;
	void Start() {
		LoadAvatar();
	}

	[System.NonSerialized] public float[] poseValues;
	private HumanPoseHandler humanPoseHandler;
	private HumanPose humanPose;
	private Transform[] boneTransforms;

	// call this if you change the animator
	public void LoadAvatar() {
		poseValues = new float[7+HumanTrait.MuscleCount];
		humanPoseHandler = new HumanPoseHandler(animator.avatar, animator.transform);
		humanPose = new HumanPose();
		humanPose.muscles = new float[HumanTrait.MuscleCount];
		boneTransforms = new Transform[HumanTrait.BoneCount];
		for(int i=0; i<HumanTrait.BoneCount; i++)
			boneTransforms[i] = animator.GetBoneTransform((HumanBodyBones)i);
	}

	// capture human pose from the animator
	public void GetPose() {
		humanPoseHandler.GetHumanPose(ref humanPose);
		// compute rootT/Q manually since GetHumanPose(ref) doesn't update bodyPosition/bodyRotation in udon
		var rootQ = Quaternion.Inverse(animator.transform.rotation) * GetMassQ(boneTransforms);
		var rootT = animator.transform.InverseTransformPoint(GetMassT(boneTransforms)) / animator.humanScale;
		poseValues[0] = rootT.x;
		poseValues[1] = rootT.y;
		poseValues[2] = rootT.z;
		poseValues[3] = rootQ.x;
		poseValues[4] = rootQ.y;
		poseValues[5] = rootQ.z;
		poseValues[6] = rootQ.w;
		System.Array.Copy(humanPose.muscles, 0, poseValues, 7, humanPose.muscles.Length);
	}

	// serialize a 1-frame clip from the capture
	public string ExportClip() {
		var curveNames = joinCurveNames.Split('|');
		var buffer = new string[curveNames.Length+2];
		buffer[0] = formatClipHead;
		for(int i=0; i<curveNames.Length; i++)
			buffer[i+1] = string.Format(formatCurveSingleFrame, curveNames[i], poseValues[i]);
		buffer[curveNames.Length+1] = string.Format(formatClipTail, 0f, keepOriginalPositionXZ ? 1 : 0);
		return string.Join("", buffer);
	}

	// from uvw.js
	Quaternion GetMassQ(Transform[] boneTransforms) {
		var leftUpperArmT  = boneTransforms[(int)HumanBodyBones.LeftUpperArm].position;
		var rightUpperArmT = boneTransforms[(int)HumanBodyBones.RightUpperArm].position;
		var leftUpperLegT  = boneTransforms[(int)HumanBodyBones.LeftUpperLeg].position;
		var rightUpperLegT = boneTransforms[(int)HumanBodyBones.RightUpperLeg].position;
		var x = ((rightUpperArmT+rightUpperLegT) - (leftUpperArmT+ leftUpperLegT));
		var y = (( leftUpperArmT+rightUpperArmT) - (leftUpperLegT+rightUpperLegT));
		return Quaternion.LookRotation(Vector3.Cross(x, y), y);
	}
	Vector3 GetMassT(Transform[] boneTransforms) {
		var vec = Vector3.zero;
		var sum = 0f;
		for(var i=0; i<defaultBoneMasses.Length; i++) {
			var bone = boneTransforms[i];
			if(bone) {
				var mass = defaultBoneMasses[i];
				vec += bone.position * mass;
				sum += mass;
			}
		}
		return vec/sum;
	}
	const string formatClipHead = @"%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!74 &7400000
AnimationClip:
  serializedVersion: 6
  m_FloatCurves:
";
	const string formatCurveSingleFrame = @"  - {{curve: {{m_Curve: [{{value: {1,7:F4} }}]}}, classID: 95, attribute: {0} }}
";
	const string formatClipTail = @"  m_AnimationClipSettings:
    serializedVersion: 2
    m_StopTime: {0:F4}
    m_LoopBlendOrientation: 1
    m_LoopBlendPositionY: 1
    m_LoopBlendPositionXZ: 1
    m_KeepOriginalOrientation: 1
    m_KeepOriginalPositionY: 1
    m_KeepOriginalPositionXZ: {1}
";
	const string joinCurveNames = "RootT.x|RootT.y|RootT.z|RootQ.x|RootQ.y|RootQ.z|RootQ.w|Spine Front-Back|Spine Left-Right|Spine Twist Left-Right|Chest Front-Back|Chest Left-Right|Chest Twist Left-Right|UpperChest Front-Back|UpperChest Left-Right|UpperChest Twist Left-Right|Neck Nod Down-Up|Neck Tilt Left-Right|Neck Turn Left-Right|Head Nod Down-Up|Head Tilt Left-Right|Head Turn Left-Right|Left Eye Down-Up|Left Eye In-Out|Right Eye Down-Up|Right Eye In-Out|Jaw Close|Jaw Left-Right|Left Upper Leg Front-Back|Left Upper Leg In-Out|Left Upper Leg Twist In-Out|Left Lower Leg Stretch|Left Lower Leg Twist In-Out|Left Foot Up-Down|Left Foot Twist In-Out|Left Toes Up-Down|Right Upper Leg Front-Back|Right Upper Leg In-Out|Right Upper Leg Twist In-Out|Right Lower Leg Stretch|Right Lower Leg Twist In-Out|Right Foot Up-Down|Right Foot Twist In-Out|Right Toes Up-Down|Left Shoulder Down-Up|Left Shoulder Front-Back|Left Arm Down-Up|Left Arm Front-Back|Left Arm Twist In-Out|Left Forearm Stretch|Left Forearm Twist In-Out|Left Hand Down-Up|Left Hand In-Out|Right Shoulder Down-Up|Right Shoulder Front-Back|Right Arm Down-Up|Right Arm Front-Back|Right Arm Twist In-Out|Right Forearm Stretch|Right Forearm Twist In-Out|Right Hand Down-Up|Right Hand In-Out|LeftHand.Thumb.1 Stretched|LeftHand.Thumb.Spread|LeftHand.Thumb.2 Stretched|LeftHand.Thumb.3 Stretched|LeftHand.Index.1 Stretched|LeftHand.Index.Spread|LeftHand.Index.2 Stretched|LeftHand.Index.3 Stretched|LeftHand.Middle.1 Stretched|LeftHand.Middle.Spread|LeftHand.Middle.2 Stretched|LeftHand.Middle.3 Stretched|LeftHand.Ring.1 Stretched|LeftHand.Ring.Spread|LeftHand.Ring.2 Stretched|LeftHand.Ring.3 Stretched|LeftHand.Little.1 Stretched|LeftHand.Little.Spread|LeftHand.Little.2 Stretched|LeftHand.Little.3 Stretched|RightHand.Thumb.1 Stretched|RightHand.Thumb.Spread|RightHand.Thumb.2 Stretched|RightHand.Thumb.3 Stretched|RightHand.Index.1 Stretched|RightHand.Index.Spread|RightHand.Index.2 Stretched|RightHand.Index.3 Stretched|RightHand.Middle.1 Stretched|RightHand.Middle.Spread|RightHand.Middle.2 Stretched|RightHand.Middle.3 Stretched|RightHand.Ring.1 Stretched|RightHand.Ring.Spread|RightHand.Ring.2 Stretched|RightHand.Ring.3 Stretched|RightHand.Little.1 Stretched|RightHand.Little.Spread|RightHand.Little.2 Stretched|RightHand.Little.3 Stretched";
	float[] defaultBoneMasses = new float[] {
		0.14545456f,
		0.12121213f,
		0.12121213f,
		0.048484854f,
		0.048484854f,
		0.009696971f,
		0.009696971f,
		0.030303033f,
		0.2909091f,
		0.012121214f,
		0.048484854f,
		0.006060607f,
		0.006060607f,
		0.024242427f,
		0.024242427f,
		0.01818182f,
		0.01818182f,
		0.006060607f,
		0.006060607f,
		0.0024242427f,
		0.0024242427f,
	};
}
}
#endif